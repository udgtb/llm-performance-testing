FROM tensorflow/tensorflow:latest-gpu-jupyter
RUN apt-get update && apt-get upgrade -y

RUN apt-get update
RUN DEBIAN_FRONTEND="noninteractive" apt-get install --yes --fix-missing \
    bash \
    build-essential \
    ca-certificates \
    curl \
    htop \
    locales \
    man \
    python3 \
    python3-pip \
    software-properties-common \
    sudo \
    systemd \
    systemd-sysv \
    unzip \
    vim \
    openssh-client \
    ffmpeg \
    wget
RUN add-apt-repository ppa:git-core/ppa
RUN DEBIAN_FRONTEND="noninteractive" apt-get install --yes git
RUN apt-get clean && apt-get autoremove -y

# RUN apt-get install python3-venv -y

# ENV VIRTUAL_ENV=/opt/venv
# RUN python -m venv $VIRTUAL_ENV
# ENV PATH="$VIRTUAL_ENV/bin:$PATH"

# [Optional] If your pip requirements rarely change, uncomment this section to add them to the image.
COPY requirements.txt /tmp/pip-tmp/
# RUN pip3 --disable-pip-version-check --no-cache-dir install -r /tmp/pip-tmp/requirements.txt \
#    && rm -rf /tmp/pip-tmp
RUN pip install --upgrade pip
RUN pip install numpy torch[dynamo] torchvision torchaudio --force-reinstall

RUN pip3 --disable-pip-version-check --no-cache-dir install -r /tmp/pip-tmp/requirements.txt \
    && rm -rf /tmp/pip-tmp


RUN useradd coder \
    --create-home \
    --shell=/bin/bash \
    --uid=1000 \
    --user-group && \
    echo "coder ALL=(ALL) NOPASSWD:ALL" >>/etc/sudoers.d/nopasswd

USER coder
ARG HOME=/home/coder

WORKDIR $HOME
ARG HOME=/home/coder

WORKDIR $HOME
# Add path
RUN export PATH="$HOME/.local/bin:$PATH"

RUN mkdir -p /home/coder/examples && cd /home/coder/examples && git clone https://coder:v4G_Pa_pVzjTgDLCjLW9@git.scc.kit.edu/von-bis/interne-software-projekte/gpt/nlpcloud.git
RUN mkdir $HOME/.vscode && \
    echo '{ \
    "recommendations": [ \
    "github.copilot", \
    "ms-toolsai.jupyter", \
    "ms-python.python", \
    "ms-python.pylint", \
    "ms-python.vscode-pylance", \
    "ms-toolsai.jupyter-keymap" \
    ] \
    }' > $HOME/.vscode/extensions.json

