# Use the TensorFlow with GPU and Jupyter base image
FROM tensorflow/tensorflow:latest-gpu-jupyter

# Update and upgrade packages in the base image
RUN apt-get update && apt-get upgrade -y

# Set the working directory to /LLM_Test
WORKDIR /LLM_Test

# make a directory for the output
RUN mkdir /LLM_Test/output

# Copy the Python test script and environment file to the container
COPY Test_LLM_Performance.py /LLM_Test
COPY Models.env /LLM_Test

# Copy the requirements.txt file to a temporary directory
COPY requirements.txt /tmp/pip-tmp/

# Upgrade pip and install specific Python packages
RUN pip install --upgrade pip
RUN pip install numpy torch[dynamo] torchvision torchaudio --force-reinstall

# Install Python dependencies listed in requirements.txt
RUN pip install -r /tmp/pip-tmp/requirements.txt

# Specify the default command to run your Python test script and direct its output to a log file
CMD ["python", "Test_LLLM_Performance.py", ">>", "/LLM_Test/output/output.log"]
